#!/usr/bin/env python3

# Cookie Clicker last best price calculator
# <orteil.dashnet.org/cookieclicker/>

import pandas as pd

df = pd.read_csv('prices.csv', index_col='name')

df['price_per_production'] = df.loc[:, 'price'] / df.loc[:, 'production']
df['max_price'] = df.loc[:, 'production'] * df.ix[-1, 'price_per_production']

print(df)
